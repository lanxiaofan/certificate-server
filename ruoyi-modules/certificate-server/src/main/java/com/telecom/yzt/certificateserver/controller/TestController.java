package com.telecom.yzt.certificateserver.controller;

import com.telecom.yzt.certificateserver.utils.PasswordUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 91274
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("/test")
public class TestController {
    @GetMapping("/test")
    public void test(){
        String s = PasswordUtil.randomPassword(8);
        System.out.println("密码生成"+s);
    }
}
